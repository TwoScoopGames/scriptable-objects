﻿using System;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects {
  [CreateAssetMenu(menuName = "ScriptableObjects/Timer")]
  public class Timer : ScriptableObject {

    [NonSerialized]
    private TimeSpan elapsed;

    [NonSerialized]
    private DateTime? started;

    void OnEnable() {
      this.hideFlags = HideFlags.DontUnloadUnusedAsset;
    }

    public void Start() {
      if (!started.HasValue) {
        started = DateTime.UtcNow;
      }
    }

    public void Stop() {
      if (started.HasValue) {
        elapsed += DateTime.UtcNow - started.Value;
        started = null;
      }
    }

    public void Reset() {
      elapsed = TimeSpan.Zero;
      started = null;
    }

    public TimeSpan Elapsed {
      get {
        if (started.HasValue) {
          return elapsed + (DateTime.UtcNow - started.Value);
        } else {
          return elapsed;
        }
      }
    }

    public bool Started {
      get {
        return started.HasValue;
      }
    }
  }
}
