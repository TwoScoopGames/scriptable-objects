using TwoScoopGames.ScriptableObjects.Events;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Events {
  [CreateAssetMenu(menuName="Events/TransformGameEvent")]
  public class TransformGameEvent: GameEvent<Transform> {}
}
