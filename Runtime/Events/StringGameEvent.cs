using System.Collections.Generic;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Events {
  [CreateAssetMenu(menuName="Events/StringGameEvent")]
  public class StringGameEvent : GameEvent<string> {}
}
