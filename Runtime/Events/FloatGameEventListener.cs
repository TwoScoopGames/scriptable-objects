using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  public class FloatGameEventListener : GameEventListener<float> {
    public FloatGameEvent subject;
    public FloatUnityEvent response;

    protected override GameEvent<float> GetSubject() {
      return subject;
    }

    protected override UnityEvent<float> GetResponse() {
      return response;
    }
  }
}
