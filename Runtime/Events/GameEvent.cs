using System.Collections.Generic;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Events {
  [CreateAssetMenu(menuName="Events/GameEvent")]
  public class GameEvent : ScriptableObject {

    private readonly List<GameEventListener> listeners = new List<GameEventListener>();

    [Multiline]
    [SerializeField]
    private string DeveloperDescription;

    public void Raise() {
      for (var i = listeners.Count - 1; i >= 0; i--) {
        listeners[i].OnEventRaised();
      }
    }

    public void RegisterListener(GameEventListener listener) {
      if (!listeners.Contains(listener)) {
        listeners.Add(listener);
      }
    }

    public bool UnregisterListener(GameEventListener listener) {
      return listeners.Remove(listener);
    }
  }

  public class GameEvent<T> : ScriptableObject {

    private readonly List<GameEventListener<T>> listeners = new List<GameEventListener<T>>();

    [Multiline]
    [SerializeField]
    private string DeveloperDescription;

    public void Raise(T value) {
      for (var i = listeners.Count - 1; i >= 0; i--) {
        listeners[i].OnEventRaised(value);
      }
    }

    public void RegisterListener(GameEventListener<T> listener) {
      if (!listeners.Contains(listener)) {
        listeners.Add(listener);
      }
    }

    public bool UnregisterListener(GameEventListener<T> listener) {
      return listeners.Remove(listener);
    }
  }
}
