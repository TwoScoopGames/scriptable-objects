using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.ScriptableObjects.Events {
  public class GameEventListener : MonoBehaviour {

    public GameEvent subject;
    public UnityEvent response;

    private void OnEnable() {
      subject.RegisterListener(this);
    }

    private void OnDisable() {
      subject.UnregisterListener(this);
    }

    public void OnEventRaised() {
      response.Invoke();
    }
  }

  public abstract class GameEventListener<T> : MonoBehaviour {

    protected abstract GameEvent<T> GetSubject();
    protected abstract UnityEvent<T> GetResponse();

    private void OnEnable() {
      GetSubject().RegisterListener(this);
    }

    private void OnDisable() {
      GetSubject().UnregisterListener(this);
    }

    public void OnEventRaised(T eventValue) {
      GetResponse().Invoke(eventValue);
    }
  }
}
