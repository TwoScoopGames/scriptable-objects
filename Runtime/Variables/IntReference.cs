﻿using System;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [Serializable]
  public class IntReference : VariableReference<int, IntVariable> {}
}
