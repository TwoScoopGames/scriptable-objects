﻿using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [CreateAssetMenu(menuName="Variables/StringVariable")]
  public class StringVariable : Variable<string> { }
}
