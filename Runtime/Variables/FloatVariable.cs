﻿using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [CreateAssetMenu(menuName="Variables/FloatVariable")]
  public class FloatVariable : Variable<float> { }
}
