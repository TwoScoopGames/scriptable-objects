﻿using System;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [Serializable]
  public class BoolReference : VariableReference<bool, BoolVariable> {}
}
