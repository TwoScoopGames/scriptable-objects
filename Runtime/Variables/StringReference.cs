﻿using System;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [Serializable]
  public class StringReference : VariableReference<string, StringVariable> {}
}
