﻿using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [CreateAssetMenu(menuName="Variables/IntVariable")]
  public class IntVariable : Variable<int> { }
}
