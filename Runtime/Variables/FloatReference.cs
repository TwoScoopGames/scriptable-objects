﻿using System;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [Serializable]
  public class FloatReference : VariableReference<float, FloatVariable> {}
}
