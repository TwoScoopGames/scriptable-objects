﻿using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Variables {
  [CreateAssetMenu(menuName="Variables/BoolVariable")]
  public class BoolVariable : Variable<bool> { }
}
