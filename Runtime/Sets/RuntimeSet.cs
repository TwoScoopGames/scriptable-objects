using System.Collections.Generic;
using UnityEngine;

namespace TwoScoopGames.ScriptableObjects.Sets {
  public abstract class RuntimeSet<T> : ScriptableObject {
    public List<T> items = new List<T>();

    public delegate void RuntimeSetMembershipEventHandler(T sender);
    public event RuntimeSetMembershipEventHandler onAdd;
    public event RuntimeSetMembershipEventHandler onRemove;

    public void Add(T item) {
      if (!items.Contains(item)) {
        items.Add(item);
        ItemAdded(item);
        onAdd?.Invoke(item);
      }
    }

    protected virtual void ItemAdded(T item) {}

    public void Remove(T item) {
      if (items.Contains(item)) {
        items.Remove(item);
        ItemRemoved(item);
        onRemove?.Invoke(item);
      }
    }

    protected virtual void ItemRemoved(T item) {}
  }
}
