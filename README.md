# Scriptable Objects

Wire up your game by using the Unity Editor as a dependency injection framework.

Based on [Ryan Hipple's Unity Austin 2017 talk Game Architecture with Scriptable
Objects](https://www.youtube.com/watch?v=raQ3iHhE_Kk). Here's [Ryan's original
code](https://github.com/roboryantron/Unite2017). I changed it a bit to use
generics to reduce some boilerplate.

# Installation

## For Unity 2019.3 or Later

[Official instructions](https://docs.unity3d.com/Manual/upm-ui-giturl.html)

1. Open the Package Manager by selecting `Window` -> `Package Manager` from the
   Unity menu.
2. Click the `+` button on the top-left corner of the Package Manager tab then
   select `Add package from git URL`.
3. Enter `https://gitlab.com/TwoScoopGames/scriptable-objects.git`, and
   click `Add`

## For Unity pre-2019.3

Add this package's git URL to your `Packages/manifest.json` file:

```json
{
  "dependencies": {
    "com.twoscoopgames.scriptableobjects": "https://gitlab.com/TwoScoopGames/scriptable-objects.git"
  }
}
```

# Usage

Watch [Ryan Hipple's Unity Austin 2017 talk Game Architecture with Scriptable
Objects](https://www.youtube.com/watch?v=raQ3iHhE_Kk). He does a good job
illustrating how to use this.

## Variables Example

1. Right click in your Project tab, and select `Create` -> `Variables` ->
   `IntVariable` from the menu. Name it "Player Health". Set the Initial Value
to 10 in the inspector.

2. Create an empty GameObject in the Hierarchy named "Player".
3. Create a new script called "DestroyOnDeath.cs" attached to the Player with
   this code:

```csharp
using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;

public class DestroyOnDeath : MonoBehaviour {
  public IntReference playersHealth;

  void Update() {
    if (playersHealth.Value <= 0) {
      Destroy(gameObject);
    }
  }
}
```

4. Set the `playersHealth` property on the DestroyOnDeath component to "Use
   Reference" and drag in the "Player Health" variable created in step 1.
5. Create a Button in the Hierarchy named "Damage".
6. Create a new script called "DamageHealth.cs" attached to the Damage Button
   with this code:

```csharp
using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;

public class DamageHealth : MonoBehaviour {
  public IntReference playersHealth;
  public int damageAmount = 5;

  public void Damage() {
    playersHealth.Value -= damageAmount;
  }
}
```

7. Set the `playersHealth` property on the DamageHealth component to "Use
   Reference" and drag in the "Player Health" variable created in step 1.
8. Set the Damage Buttons `onClick` property to call the `Damage()` function on itself.
9. Click the "Play" button.
10. Click the Damage Button twice and observe that the Player GameObject is
    destroyed.

## Events Example

1. Right click in your Project tab, and select `Create` -> `Events` ->
   `StringGameEvent` from the menu. Name it "Log Message".
2. Create an empty GameObject in the Hierarchy named "Logger".
3. Create a new script called "Logger.cs" attached to the Logger GameObject with
   this code:

```csharp
using UnityEngine;
using TwoScoopGames.ScriptableObjects.Events;

public class Logger : MonoBehaviour {
  public StringGameEvent logEvent;

  public void Log(string message) {
    Debug.Log(message);
  }
}
```

4. Drag in the "Log Message" event into the "logEvent" property of the logger
   component.
5. Create a Button in the Hierarchy named "Log".
6. Set the Log Buttons `onClick` property to call the `Raise()` function on the
   "Log Message" event, and give it an argument of "hello world".
7. Click the "Play" button.
8. Click the Log Button, and observe that "hello world" has been logged to the
   console panel.

# Extending

There's a bit of boilerplate to extend this library's generic classes into
concrete implementations, but unfortunately Unity's inspector won't display
editors for generic classes.

## Variables

If you want to make a custom Variable for your own `MyClass` type you'll need to
make two classes.

MyClassVariable:

```csharp
using UnityEngine;

[CreateAssetMenu(menuName="Variables/MyClassVariable")]
public class MyClassVariable : Variable<MyClass> { }
```

MyClassVariableReference:

```csharp
using System;

[Serializable]
public class MyClassReference : VariableReference<MyClass, MyClassVariable> {}
```

## Events

If you want to make a custom GameEvent that sends your own `MyClass` type as an
argument you'll need to make three classes.

MyClassGameEvent:

```csharp
using TwoScoopGames.Events;
using UnityEngine;

[CreateAssetMenu(menuName="Events/MyClassGameEvent")]
public class MyClassGameEvent : GameEvent<MyClass> {}
```

MyClassUnityEvent:

```csharp
using System;
using UnityEngine.Events;

[Serializable]
public class MyClassUnityEvent : UnityEvent<MyClass> {}
```

MyClassGameEventListener:

```csharp
using TwoScoopGames.Events;
using UnityEngine;
using UnityEngine.Events;

public class MyClassGameEventListener : GameEventListener<MyClass> {
  public MyClassGameEvent subject;
  public MyClassUnityEvent response;

  protected override GameEvent<MyClass> GetSubject() {
    return subject;
  }

  protected override UnityEvent<MyClass> GetResponse() {
    return response;
  }
}
```
